import React from 'react';
export function componentWithConsumer(Context, Component) {
    return function WrapperComponent(props) {
        return (
            <Context.Consumer>
                {state => <Component {...props} context={state} />}
            </Context.Consumer>
        );
    };
}
import React from 'react';
const Context = React.createContext({});
export function withAppContext(Component) {
    return class AppProvider extends Component {
        constructor(props) {
            super(props);
            this.state = {
                lang: 'en'
            };
        }
         onChangeLang() {
            this.setState({ lang: 'es' });
        }
        render() {
            return (
                <Context.Provider
                    value={{
                        state: this.state,
                        onChangeLang: this.onChangeLang
                    }}
                >
                    <Component />
                </Context.Provider>
            );
        }
    }
}
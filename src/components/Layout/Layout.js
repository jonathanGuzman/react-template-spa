import React, {PureComponent} from 'react'

const Layout = (WrappedComponent) => {
    return class extends PureComponent {
        constructor() {
            super();
            this.state = {
            };
        }
        render() { 
            return (
            <div>
            <div>
                <h1>header</h1>
            </div>
            <WrappedComponent {...this.props} />
            <div>
                <h1>Footer</h1>
            </div>
            </div>
            )
        }
    }
}
export default Layout;
FROM node:alpine

ENV NPM_CONFIG_LOGLEVEL warn

RUN mkdir -p /opt/app

WORKDIR /opt/app

COPY . /opt/app

# Give permissions
RUN chown -R node:node /opt/app/*

# Install app dependencies
RUN npm install

# network ports
EXPOSE 3000

# Add user
USER node

CMD [ "npm", "start" ]




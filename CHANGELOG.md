# 0.1.0 (2019-06-22)


### Features

* context integration ([f5598d7](https://gitlab.com/jonathanGuzman/react-template-spa/commit/f5598d7))
* support Layout custom components on router ([8c6a277](https://gitlab.com/jonathanGuzman/react-template-spa/commit/8c6a277))
* **router:** add router configuration ([3aec805](https://gitlab.com/jonathanGuzman/react-template-spa/commit/3aec805))



